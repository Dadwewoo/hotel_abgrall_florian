<?php
/* 
On souhaite développer un programme qui permet la gestion des réservations de chambre pour un hôtel qui contient 20 chambres. Les fonctions à assurer sont :
-	Au lancement de notre programme, on crée une liste de chambres, qui sont réservées (1) ou libres (0) en utilisant la fonction randInt.
-	Afficher l’état de l’hôtel
-	Afficher le nombre de chambres réservées 
-	Afficher le nombre de chambres libres  
-	Afficher le numéro de la première chambre vide
-	Afficher le numéro de la dernière chambre vide
-	Réserver une chambre 
-	Libérer une chambre 

Pour cela, nous devons afficher le menu suivant à l’utilisateur de notre programme : 

A-	Afficher l’état de l’hôtel 
B-	Afficher le nombre de chambres réservées
C-	Afficher le nombre de chambres libres
D-	Afficher le numéro de la première chambre vide
E-	Afficher le numéro de la dernière chambre vide
F-	Réserver une chambre (Le programme doit réserver la première chambre vide)
G-	Libérer une chambre (Le programme doit libérer la dernière chambre occupée) */

//      ----------------PROGRAMME-----------------   

	// Saisir les données sur les chambres d'hôtel.

$login = "admin";
$mdp = "1234";


for ($i = 1 ; $i < 21 ; $i++){

			$chambres [$i] = rand(0, 1 );

}




Do {
	
    echo (" " .PHP_EOL);
    echo (" ---------- Menu des réservations de l'hotel ----------");
    echo (" " .PHP_EOL);
    echo (" " .PHP_EOL);

	// Affichage menu.
	
		echo("(A) Afficher l'état de l'hôtel" .PHP_EOL);
		echo("(B) Afficher le nombres de chambres réservés." .PHP_EOL);
		echo("(C) Afficher le nombres de chambres libres." .PHP_EOL);
		echo("(D) Afficher le numéro de la première chambre vide." .PHP_EOL);
		echo("(E) Afficher le numéro de la dernière chambre vide." .PHP_EOL);
		echo("(F) Réserver une chambre" .PHP_EOL);
		echo("(G) Libérer une chambre" .PHP_EOL);	
		echo("(Q) Quitter" .PHP_EOL);
		
        echo (" " .PHP_EOL);
        echo (" " .PHP_EOL);
		
        //Choix menu
        

        $choix_menu=readline ("Choix : " .PHP_EOL);

    if ((preg_match("#^[a-zA-Z]+$#", $choix_menu)) && 
    ($choix_menu== "A" or $choix_menu == "B" or $choix_menu == "C" or $choix_menu == "D" or $choix_menu == "E" or $choix_menu == "F" or $choix_menu== "G" or $choix_menu== "Q")){

	//Affichage de l'état de l'hôtel.
	
	if ($choix_menu == "A" ) {
	
    echo (" " .PHP_EOL);
    echo (" ---------- Liste des réservations ----------");
    echo (" " .PHP_EOL);
    echo (" " .PHP_EOL);

		for ($i = 1 ; $i < 21 ; $i++){
				if ($chambres [$i] == 1){
	
					echo ("La chambre " .$i ." est réservée." .PHP_EOL);
						
				}	
				else if ($chambres [$i] == 0) {
					
					echo ("La chambre " .$i ." est libre." .PHP_EOL);
					
				}					
		}		
	
	}
	
    echo (" " .PHP_EOL);
	
	//Afficher le nombres de chambres réservées
	
	if ($choix_menu == "B" ) {
    $cpt = 0;

    echo (" " .PHP_EOL);
    echo (" ---------- Liste des chambres réservées ----------");
    echo (" " .PHP_EOL);
    echo (" " .PHP_EOL);

		for ($i = 1 ; $i < 21 ; $i++){
				if ($chambres [$i] == 1){
					
					$cpt += 1;
					echo ("La chambre " .$i ." est réservée." .PHP_EOL);
						
				}
		}
	
    echo (" " .PHP_EOL);
	echo ($cpt ." chambres réservées." .PHP_EOL);
	echo (" " .PHP_EOL);
	
	}
	
	//Afficher le nombres de chambres libres

	if ($choix_menu == "C") {
    $cpt = 0;

    echo (" " .PHP_EOL);
    echo (" ---------- Liste des chambres libres ----------");
    echo (" " .PHP_EOL);
    echo (" " .PHP_EOL);

		for ($i = 1 ; $i < 21 ; $i++){
				if ($chambres [$i] == 0){	
					
					$cpt2 += 1;
					echo ("La chambre " .$i ." est libre." .PHP_EOL);
					
				}
		}

	echo (" " .PHP_EOL);
	echo ($cpt2 ." chambres libres." .PHP_EOL);
    echo (" " .PHP_EOL);		
		
	}
	
	//Afficher le numéro de la première chambre vide
	
	if ($choix_menu == "D") {
		
		for ($i = 1 ; $i < 21 ; $i++){
				if ($chambres [$i] == 0){
                    
                    echo (" " .PHP_EOL);
                    echo ("La chambre " .$i ." est la première chambre vide." .PHP_EOL);
                    echo (" " .PHP_EOL);

					break;
						
				}
			
		}		
		
	}
	
	//Afficher le numéro de la dernière chambre vide
	
	if ($choix_menu == "E") {
		
		for ($i = 20 ; $i > 0 ; $i--){
				if ($chambres [$i] == 0){

                    echo (" " .PHP_EOL);
                    echo ("La chambre " .$i ." est la dernière chambre vide." .PHP_EOL);
                    echo (" " .PHP_EOL);

					break;
						
				}
			
		}	
		
	}
	
	//----- Réserver une chambre ----- 
	
	if ($choix_menu == "F") {
		
	echo (" " .PHP_EOL);
		
		$validLogin=readline ("Veuillez saisir votre login : ");
		$validMdp=readline ("Veuillez saisir votre mot de passe : ");
	
		if ($validLogin == $login && $validMdp == $mdp){

			for ($i = 1 ; $i < 21 ; $i++){

			$sauvegarde_chambre = null;
	
					if ($chambres[$i] == 0){

						$sauvegarde_chambre = $i;

					break;
							
					}	
			}	

			if ($sauvegarde_chambre == null) {
	
				echo(" ---------- Hotel complet ---------- ");

			continue;

			}

            do  {

		        $validation=readline ("Voulez-vous réserver la chambre " .$sauvegarde_chambre ." ? (Oui / Non)" .PHP_EOL);
                
                if (strtolower($validation) == "non") {
				
				break;

				}
			    elseif (strtolower($validation) == "oui") {
                        
                        $chambres[$sauvegarde_chambre] = 1;
                        echo (" " .PHP_EOL);
                        echo ("La chambre " .$sauvegarde_chambre ." est maintenant réservée." .PHP_EOL);
						echo (" " .PHP_EOL);
				break;
		
			    }

                

            } while (1>0);
		}
		else{
			echo("Login ou mot de passe invalide.");
		}
		
	}
	
	//----- Libérer une chambre ----- 

	if ($choix_menu == "G") {
		
	echo (" " .PHP_EOL);
	

		$validLogin=readline ("Veuillez saisir votre login : ");
        $validMdp=readline ("Veuillez saisir votre mot de passe : ");
        
		if ($validLogin == $login && $validMdp == $mdp){

			for ($i = 20 ; $i > 0 ; $i--){

			$sauvegarde_chambre = null;
	
					if ($chambres[$i] == 1){

						$sauvegarde_chambre = $i;

					break;
							
					}	
			}	

			if ($sauvegarde_chambre == null) {
	
				echo(" ---------- Hotel complétement libre ---------- ");

			continue;

			}

            do  {

		        $validation=readline ("Voulez-vous libérer la chambre " .$sauvegarde_chambre ." ? (Oui / Non)" .PHP_EOL);
                
                if (strtolower($validation) == "non") {
				
				break;

				}
			    elseif (strtolower($validation) == "oui") {
                        
                        $chambres[$sauvegarde_chambre] = 0;
                        echo (" " .PHP_EOL);
                        echo ("La chambre " .$sauvegarde_chambre ." est maintenant libérée." .PHP_EOL);
						echo (" " .PHP_EOL);
				break;
		
			    }

                

            } while (1>0);
		}
		else{
			echo("Login ou mot de passe invalide.");
		}
		
	}
	
	//----- Sortie ----- 

	if ($choix_menu == "Q"){

		break;

    }
}
else {

    echo ("Saisie invalide, veuillez saisir une des lettres indiqué à la base des choix du menu.".PHP_EOL);
    echo ("   ".PHP_EOL);

}
	
} while (true);