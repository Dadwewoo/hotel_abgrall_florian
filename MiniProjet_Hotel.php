<?php
/* 
On souhaite développer un programme qui permet la gestion des réservations de chambre pour un hôtel qui contient 20 chambres. Les fonctions à assurer sont :
-	Au lancement de notre programme, on crée une liste de chambres, qui sont réservées (1) ou libres (0) en utilisant la fonction randInt.
-	Afficher l’état de l’hôtel
-	Afficher le nombre de chambres réservées 
-	Afficher le nombre de chambres libres  
-	Afficher le numéro de la première chambre vide
-	Afficher le numéro de la dernière chambre vide
-	Réserver une chambre 
-	Libérer une chambre 

Pour cela, nous devons afficher le menu suivant à l’utilisateur de notre programme : 


A-	Afficher l’état de l’hôtel 
B-	Afficher le nombre de chambres réservées
C-	Afficher le nombre de chambres libres
D-	Afficher le numéro de la première chambre vide
E-	Afficher le numéro de la dernière chambre vide
F-	Réserver une chambre (Le programme doit réserver la première chambre vide)
G-	Libérer une chambre (Le programme doit libérer la dernière chambre occupée) */

//      ----------------PROGRAMME-----------------   //

	// Saisir les données sur les chambres d'hôtel.

$login = "admin";
$mdp = "1234";

for ($i = 1 ; $i < 21 ; $i++){

			$chambres [$i] = rand(0, 1 );
			// readline ("Saisir l'état de la chambre " .$i ."(libre (0) réservée (1)) =>  ");
 
}

Do {
	
	PHP_EOL;

	// Affichage menu.
	
		echo("(A) Afficher l'état de l'hôtel" .PHP_EOL);
		echo("(B) Afficher le nombres de chambres réservés." .PHP_EOL);
		echo("(C) Afficher le nombres de chambres libres." .PHP_EOL);
		echo("(D) Afficher le numéro de la première chambre vide." .PHP_EOL);
		echo("(E) Afficher le numéro de la dernière chambre vide." .PHP_EOL);
		echo("(F) Réserver une chambre" .PHP_EOL);
		echo("(G) Libérer une chambre" .PHP_EOL);	
		echo("(Q) Quitter" .PHP_EOL);
		
		PHP_EOL;
		PHP_EOL;
		
			//Choix menu
			
				$choix_menu=readline ("Veuillez saisir la lettre appropriée : " .PHP_EOL);
		
		
	//Affichage de l'état de l'hôtel.
	
	if (strtoupper($choix_menu) == "A" ) {
	
	PHP_EOL;
	
		for ($i = 1 ; $i < 11 ; $i++){
				if ($chambres [$i] == 1){
	
					echo ("La chambre " .$i ." est réservée." .PHP_EOL);
						
				}	
				else if ($chambres [$i] == 0) {
					
					echo ("La chambre " .$i ." est libre." .PHP_EOL);
					
				}					
		}		
	
	}
	
	PHP_EOL;
	
	//Afficher le nombres de chambres réservées
	
	if (strtoupper($choix_menu) == "B" ) {
		
	PHP_EOL;
	
		for ($i = 1 ; $i < 11 ; $i++){
				if ($chambres [$i] == 1){
					
					$cpt += 1;
					echo ("La chambre " .$i ." est réservée." .PHP_EOL);
						
				}
		}
	
	PHP_EOL;
	echo ($cpt ." chambres réservées." .PHP_EOL);
	PHP_EOL;
	
	}
	
	//Afficher le nombres de chambres libres

	if (strtoupper($choix_menu) == "C") {
	
		for ($i = 1 ; $i < 11 ; $i++){
				if ($chambres [$i] == 0){	
					
					$cpt2 += 1;
					echo ("La chambre " .$i ." est libre." .PHP_EOL);
					
				}
		}

	PHP_EOL;
	echo ($cpt2 ." chambres libres." .PHP_EOL);
	PHP_EOL;		
		
	}
	
	//Afficher le numéro de la première chambre vide
	
	if (strtoupper($choix_menu) == "D") {
		
		for ($i = 1 ; $i < 11 ; $i++){
				if ($chambres [$i] == 0){
					
					echo ("La chambre " .$i ." est la première chambre vide." .PHP_EOL);
					break;
						
				}
			
		}		
		
	}
	
	//Afficher le numéro de la dernière chambre vide
	
	if (strtoupper($choix_menu) == "E") {
		
		for ($i = 10 ; $i >= 1 ; $i--){
				if ($chambres [$i] == 0){
						
					echo ("La chambre " .$i ." est la dernière chambre vide." .PHP_EOL);
					break;
						
				}
			
		}	
		
	}
	
	//Réserver une chambre 
	
	if (strtoupper($choix_menu) == "F") {
		
	PHP_EOL;
	/*
		for ($i = 0 ; $i < 10 ; $i++){
				if ($chambres [$i] == 0) {
					
					echo ("La chambre " .$i ." est libre." .PHP_EOL);
					
				}					
		}	
	*/		
		$validLogin=readline ("Veuillez saisir votre login : ");
		$validMdp=readline ("Veuillez saisir votre mot de passe : ");
	
		if ($validLogin == $login && $validMdp == $mdp){
	/*		
		$choix_chambre_a_reserver=readline ("Choisir la chambre à reserver (entrer son numéro) : " .PHP_EOL);
	*/
		for ($i = 1 ; $i < 11 ; $i++){
				if ($chambres [$i] == 0){
					
					$sauvegarde_chambre = $i;
					break;
						
				}
			
		}	
		
		$validation=readline ("Voulez-vous réserver la chambre " .$sauvegarde_chambre ." ? (Oui / Non)" .PHP_EOL);
		
			if (strtolower($validation) == "oui") {
			
				$chambres[$sauvegarde_chambre] = 1;
				echo ("La chambre " .$sauvegarde_chambre ." est maintenant réservée." .PHP_EOL);
		
			}
		}
		else {
		
			echo("Login ou mot de passe invalide" .PHP_EOL);
		
		}
	}
	
	//Libérer une chambre 

	if (strtoupper($choix_menu) == "G") {
		
	PHP_EOL;
	
	/*	for ($i = 0 ; $i < 10 ; $i++){
				if ($chambres [$i] == 1){
	
					echo ("La chambre " .$i ." est réservée." .PHP_EOL);
						
				}				
		}	
	*/	
		$validLogin=readline ("Veuillez saisir votre login : ");
		$validMdp=readline ("Veuillez saisir votre mot de passe : ");
		if ($validLogin == $login && $validMdp == $mdp){
	/*		
			$choix_chambre_a_liberer=readline ("Choisir la chambre à libérer (entrer son numéro) : " .PHP_EOL);
	*/
		for ($i = 10 ; $i >= 1 ; $i--){
				if ($chambres [$i] == 1){
						
					$sauvegarde_chambre = $i;
					break;
						
				}
			
		}	
		
			$validation=readline ("Voulez-vous libérer la chambre " .$sauvegarde_chambre ." ? (Oui / Non)" .PHP_EOL);
		
			if (strtolower($validation) == "oui") {
			
				$chambres[$sauvegarde_chambre] = 0;
				echo ("La chambre " .$sauvegarde_chambre ." est maintenant libre." .PHP_EOL);
		
			}
		}
		else {
		
			echo("Login ou mot de passe invalide" .PHP_EOL);
		
		}
	}
	
	//Sortie

	if (strtoupper($choix_menu) == "Q"){

		break;

	}
	
} while (true);